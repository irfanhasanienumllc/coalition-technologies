<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $path = storage_path() . "/app/products.json";
    $products = json_decode(file_get_contents($path), true);
    return view('welcome', compact('products'));
});

Route::post('products','ProductController@store')->name('products.store');

Route::get('products','ProductController@getJson')->name('products.json');




