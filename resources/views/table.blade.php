@php
$sum = 0;
@endphp
@foreach($products as $product)
    <tr>
        <th></th>
        <td>{{$product['name']}}</td>
        <td>{{$product['quantity']}}</td>
        <td>{{ $product['price'] }}</td>
        <td>{{ $product['datetime_submitted'] }}</td>
        <td>{{$product['price']*$product['quantity']}}</td>
    </tr>
    @php
    $sum+= $product['price']*$product['quantity']
    @endphp
@endforeach

<tr>
    <th></th>
    <td></td>
    <td></td>
    <td></td>
    <td>Total</td>
    <td>{{$sum}}</td>
</tr>
