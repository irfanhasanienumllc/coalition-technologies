<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <title>Laravel</title>

        <!-- Fonts -->
    </head>
    <body>
    <div class="container">
    <form action="/products" method="post" id="store-products">
        {{csrf_field()}}
        <div class="form-group">
            <label for="email">Product Name: </label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="email">Quantity in stock: </label>
            <input type="number" class="form-control" id="quantity" name="quantity">
        </div>
        <div class="form-group">
            <label for="email">Price per item: </label>
            <input type="number" class="form-control" id="price" name="price">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
        <div class="list-of-products">
            <h1>List of all products</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Quantity in stock</th>
                    <th scope="col">Price</th>
                    <th scope="col">Datetime submitted</th>
                    <th scope="col">Total value number</th>
                </tr>
                </thead>
                <tbody id="list-of-all-items">
                @include('table',['products'=>$products])
                </tbody>
            </table>
        </div>
    </div>

    </body>

    <script>
        $('#store-products').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                headers: {'X-CSRF-Token': $('input[name=_token]').val()},
                type: "POST",
                url: this.action,
                data: $(this).serialize(),
                success: function (data) {
                    $('#list-of-all-items').html(data);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    </script>
</html>
