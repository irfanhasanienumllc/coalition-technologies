<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    //
    public function store(Request $request)
    {
        try {
            $file = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) : [];

            $inputData = $request->only(['name', 'price', 'quantity']);

            $inputData['datetime_submitted'] = date('Y-m-d H:i:s');

            array_push($file,$inputData);

            Storage::disk('local')->put('products.json', json_encode($file));

            $products = $this->getJson();

            return view('table',compact('products'));

        } catch(Exception $e) {

            return ['error' => true, 'message' => $e->getMessage()];

        }

    }

    public function getJson()
    {
        $path = storage_path() . "/app/products.json";
        $products = json_decode(file_get_contents($path), true);
        $sum =0;
        foreach ($products as $product)
        {
            $sum+=$product['price']*$product['quantity'];
        }
        $products['sum']=$sum;
        return $products;
    }
}
